<?php
// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Contains all Wordpress jQuery Plugins with links to explanation

$shoprex_jquery_main_plugins = array(
	array( "display_name" => "jquery", "external_link" => "http://jquery.com/" ),
	array( "display_name" => "jquery-ui-core", "external_link" => "http://jqueryui.com/" ),
	array( "display_name" => "jquery-effects-core", "external_link" => "http://jqueryui.com/effect/" ),
);

$shoprex_jquery_plugins = array(
	array( "display_name" => "jquery-form", "external_link" => "http://plugins.jquery.com/project/form/" ),
	array( "display_name" => "jquery-color", "external_link" => "http://plugins.jquery.com/project/color/" ),
	array( "display_name" => "jquery-masonry", "external_link" => "http://masonry.desandro.com/" ),
	array( "display_name" => "wp-mediaelement", "external_link" => "http://mediaelementjs.com/" ),
	array(
		"display_name"  => "schedule",
		"external_link" => "http://trainofthoughts.org/blog/2007/02/15/jquery-plugin-scheduler/"
	),
	array(
		"display_name"  => "suggest",
		"external_link" => "https://web.archive.org/web/20111017233444/http://plugins.jquery.com/project/suggest"
	),
	array(
		"display_name"  => "hoverIntent",
		"external_link" => "http://cherne.net/brian/resources/jquery.hoverIntent.html"
	),
	array( "display_name" => "jquery-hotkeys", "external_link" => "http://plugins.jquery.com/project/hotkeys" ),
);

// Todo: Don't work
// array("display_name" => "iris", "external_link" => "https://github.com/automattic/Iris"),

$shoprex_jquery_ui_plugins = array(
	array( "display_name" => "jquery-ui-widget", "external_link" => "http://jqueryui.com/widget/" ),
	array( "display_name" => "jquery-ui-accordion", "external_link" => "http://jqueryui.com/demos/accordion/" ),
	array( "display_name" => "jquery-ui-autocomplete", "external_link" => "http://jqueryui.com/demos/autocomplete/" ),
	array( "display_name" => "jquery-ui-button", "external_link" => "http://jqueryui.com/demos/button/" ),
	array( "display_name" => "jquery-ui-datepicker", "external_link" => "http://jqueryui.com/demos/datepicker/" ),
	array( "display_name" => "jquery-ui-dialog", "external_link" => "http://jqueryui.com/demos/dialog/" ),
	array( "display_name" => "jquery-ui-draggable", "external_link" => "http://jqueryui.com/demos/draggable/" ),
	array( "display_name" => "jquery-ui-droppable", "external_link" => "http://jqueryui.com/demos/droppable/" ),
	array( "display_name" => "jquery-ui-menu", "external_link" => "http://jqueryui.com/menu/" ),
	array( "display_name" => "jquery-ui-mouse", "external_link" => "" ),
	array( "display_name" => "jquery-ui-position", "external_link" => "http://jqueryui.com/demos/position/" ),
	array( "display_name" => "jquery-ui-progressbar", "external_link" => "http://jqueryui.com/demos/progressbar/" ),
	array( "display_name" => "jquery-ui-selectable", "external_link" => "http://jqueryui.com/demos/selectable/" ),
	array( "display_name" => "jquery-ui-resizable", "external_link" => "http://jqueryui.com/demos/resizable/" ),
	array( "display_name" => "jquery-ui-selectmenu", "external_link" => "http://jqueryui.com/selectmenu/" ),
	array( "display_name" => "jquery-ui-sortable", "external_link" => "http://jqueryui.com/demos/sortable/" ),
	array( "display_name" => "jquery-ui-slider", "external_link" => "http://jqueryui.com/demos/slider/" ),
	array( "display_name" => "jquery-ui-spinner", "external_link" => "http://jqueryui.com/demos/spinner/" ),
	array( "display_name" => "jquery-ui-tooltip", "external_link" => "http://jqueryui.com/demos/tooltip/" ),
	array( "display_name" => "jquery-ui-tabs", "external_link" => "http://jqueryui.com/demos/tabs/" ),
);

$shoprex_jquery_effect_plugins = array(
	array( "display_name" => "jquery-effects-blind", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-bounce", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-clip", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-drop", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-explode", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-fade", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-fold", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-highlight", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-pulsate", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-scale", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-shake", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-slide", "external_link" => "http://jqueryui.com/effect/" ),
	array( "display_name" => "jquery-effects-transfer", "external_link" => "http://jqueryui.com/effect/" ),
);