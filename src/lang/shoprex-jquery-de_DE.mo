��          �      l      �     �     �     �  K   �     H     M     Z     z     �     �     �  #   �  /   �  !   "  +   D     p     �     �     �     �  �  �     �     �     �  R   �            ,   &  ,   S     �     �     �  '   �  2   �       (   7     `     r     �     �     �                  	                                                        
                        All off All on Andreas Rex Enable the integrated jQuery and its plugins individually for the frontend. Link Main section Setting not saved, error code 1 Setting not saved, error code 2 Setting saved Shoprex jQuery Shoprex jQuery Enabler Shoprex jQuery and Plugins Settings You do not have permission to access this page. https://shoprex.de/en/profile.php https://shoprex.de/en/wordpress_plugins.php jQuery UI plugins jQuery effects plugins jQuery plugins off on Project-Id-Version: Shoprex jQuery Enabler 1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/shoprex_jquery
POT-Creation-Date: 2016-10-26 07:26+0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-10-26 07:32+0200
Language-Team: Andreas Rex <post@shoprex.de>
X-Generator: Poedit 1.8.11
Last-Translator: Andreas Rex <post@shoprex.de>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de_DE
X-Poedit-SourceCharset: UTF-8
 Alle aus Alle an Andreas Rex Aktivieren Sie das integrierte jQuery und seine Plugins einzeln für das Frontend. Link Hauptbereich Einstellung nicht gespeichert, Fehler Code 1 Einstellung nicht gespeichert, Fehler Code 2 Einstellung gespeichert Shoprex jQuery Shoprex jQuery Enabler Shoprex jQuery und Plugin Einstellungen Sie haben nicht die Berechtigung für diese Seite. https://shoprex.de/profil.php https://shoprex.de/wordpress_plugins.php jQuery UI Plugins jQuery Effect Plugins jQuery Plugins Aus An 