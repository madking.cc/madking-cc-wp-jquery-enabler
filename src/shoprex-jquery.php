<?php
/*
Plugin Name: Shoprex jQuery Enabler
Plugin URI: https://shoprex.de/en/wordpress_plugins.php
Description: Enable the integrated jQuery and its plugins individually for the frontend.
Version: 1.0
Author: Andreas Rex
Author URI: https://shoprex.de/en/profile.php
License: GNU GPL v2.0
Text Domain: shoprex-jquery
Domain Path: /lang/
*/

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Slug
$shoprex_slug             = "shoprex";
$shoprex_jquery_slug_menu = "shoprex_jquery_settings";

/// Helper Functions ///

// Set DB Keyname
function shoprex_jquery_format_db_keyname( $string ) {
	$string = strtolower( $string );
	$string = str_replace( "-", "_", $string );

	return $string;
}

// Function for admin submenu
function shoprex_jquery_get_select( $array ) {
	$content = "";
	foreach ( $array as $element ) {
		$shoprex_load_{strtolower( $element['display_name'] )} = get_option( "shoprex_load_" . shoprex_jquery_format_db_keyname( $element['display_name'] ), false );

		//$test = "shoprex_load_".format_db_keyname($element['display_name']);
		//var_dump($test);


		$external_link = "";
		if ( ! empty( $element["external_link"] ) ) {
			$external_link = " <a href='" . $element["external_link"] . "' target='_blank'>" . esc_html__( 'Link', 'shoprex-jquery' ) . "</a>";
		}

		$content .= "<p><span class='col1'>" . $element["display_name"] . ":</span><span>
<input class='settings-select' type='radio' value='1' name='shoprex_load_" . strtolower( $element['display_name'] ) . "'";
		if ( $shoprex_load_{strtolower( $element['display_name'] )} ) {
			$content .= " checked";
		}
		$content .= ">" . esc_html__( 'on', 'shoprex-jquery' ) . "\n
<input class='settings-select' type='radio' value='0' name='shoprex_load_" . strtolower( $element['display_name'] ) . "'";
		if ( ! $shoprex_load_{strtolower( $element['display_name'] )} ) {
			$content .= " checked";
		}
		$content .= ">" . esc_html__( 'off', 'shoprex-jquery' ) . "\n
</span>$external_link</p>\n";

	}

	return $content;
}

// Function to set Database values
function shoprex_jquery_set_db_value( $db_string, $db_value ) {
	wp_cache_delete( 'alloptions', 'options' ); // Delete Cache for correct result

	if ( ( $result = get_option( shoprex_jquery_format_db_keyname( $db_string ) ) ) !== false ) { // check if db record already exist
		if ( $result != $db_value )   // If yes, check if value has changed
		{
			$result = update_option( shoprex_jquery_format_db_keyname( $db_string ), $db_value );
		} // If yes, set new value
		else {
			return true;
		} // If no, send ok
	} else {
		$result = add_option( shoprex_jquery_format_db_keyname( $db_string ), $db_value );  // Create new record
	}

	return $result;  // return result
}

// Function to delete Database values
function shoprex_jquery_delete_db_entry( $db_string ) {
	wp_cache_delete( 'alloptions', 'options' ); // Delete Cache for correct result

	if ( ( $result = get_option( shoprex_jquery_format_db_keyname( $db_string ) ) ) !== false ) { // check if db record already exist
		delete_option( shoprex_jquery_format_db_keyname( $db_string ) );
	}

	wp_cache_delete( 'alloptions', 'options' ); // Delete Cache for correct result
}

//// Hooks ////

// Load Translation
function shoprex_jquery_load_textdomain() {
	load_plugin_textdomain( 'shoprex-jquery', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );
}

add_action( 'plugins_loaded', 'shoprex_jquery_load_textdomain' );

// Hook to load scripts and stylesheets
function shoprex_jquery_load_script_style( $hook ) {
	// Load only on ?page=mypluginname
	if ( $hook == 'settings_page_shoprex_jquery_settings' ) {

		// Include Javascript
		wp_enqueue_script( 'shoprex-jquery-save-settings', plugins_url( '/js/shoprex-save-settings.js', __FILE__ ), array( 'jquery' ), '', true );
		wp_localize_script( 'shoprex-jquery-save-settings', 'shoprex_js_translation', array(
			'security'           => wp_create_nonce( 'shoprex-jquery-nonce' ),
			'option_saved'       => esc_html__( 'Setting saved', 'shoprex-jquery' ),
			'option_not_saved_1' => esc_html__( 'Setting not saved, error code 1', 'shoprex-jquery' ),
			'option_not_saved_2' => esc_html__( 'Setting not saved, error code 2', 'shoprex-jquery' ),
		) );
		// Include CSS
		wp_enqueue_style( 'shoprex-jquery-settings', plugins_url( '/css/shoprex-jquery.css', __FILE__ ) );

	}
}

add_action( 'admin_enqueue_scripts', 'shoprex_jquery_load_script_style' );


// Callback function for hook menu
function shoprex_jquery_add_admin_submenu() {
	global $shoprex_jquery_slug_menu;

	$args = "manage_options";


	add_submenu_page( "options-general.php", esc_html__( 'Shoprex jQuery and Plugins Settings', 'shoprex-jquery' ), esc_html__( 'Shoprex jQuery', 'shoprex-jquery' ), $args, $shoprex_jquery_slug_menu, "shoprex_jquery_callback_admin_submenu" );
}

add_action( "admin_menu", "shoprex_jquery_add_admin_submenu" );

// Callback function to display settings page
function shoprex_jquery_callback_admin_submenu() {

	$content = "";

	if ( ! current_user_can( 'manage_options' ) ) {
		$content .= "<p>" . esc_html__( 'You do not have permission to access this page.', 'shoprex-jquery' ) . "</p>";
	} else {

		// Include data file
		require( plugin_dir_path( __FILE__ ) . "shoprex-data.php" );

		$content .= "<div class='wrap'>\n";
		$content .= "<div class='icon32'><br /></div>\n";
		$content .= "<h2 id='page-title'>" . esc_html__( 'Shoprex jQuery and Plugins Settings', 'shoprex-jquery' ) . " <img src='" . esc_url( admin_url() . '/images/loading.gif' ) . "' id='loading-animation' style='display:none' /></h2>\n";

		$content .= "<div class='col33p'>\n";
		$content .= "<h3>" . esc_html__( 'Main section', 'shoprex-jquery' ) . "</h3>\n";
		$content .= shoprex_jquery_get_select( $shoprex_jquery_main_plugins );
		$content .= "<button class='button' onclick='shoprex_jquery_all(true)'>" . esc_html__( 'All on', 'shoprex-jquery' ) . "</button> <button class='button' onclick='shoprex_jquery_all(false)'>" . esc_html__( 'All off', 'shoprex-jquery' ) . "</button>\n";
		$content .= "<h3>" . esc_html__( 'jQuery plugins', 'shoprex-jquery' ) . "</h3>\n";
		$content .= shoprex_jquery_get_select( $shoprex_jquery_plugins );
		$content .= "</div><div class='col33p'>\n";
		$content .= "<h3>" . esc_html__( 'jQuery UI plugins', 'shoprex-jquery' ) . "</h3>\n";
		$content .= shoprex_jquery_get_select( $shoprex_jquery_ui_plugins );
		$content .= "</div><div class='col33p'>\n";
		$content .= "<h3>" . esc_html__( 'jQuery effects plugins', 'shoprex-jquery' ) . "</h3>\n";
		$content .= shoprex_jquery_get_select( $shoprex_jquery_effect_plugins );
		$content .= "</div><div class='clear-float'></div>\n";

		$content .= "</div>\n";
	}
	echo $content;
}

// Ajax save settings page
function shoprex_jquery_save_settings() {
	// Verify Nonce
	check_ajax_referer( "shoprex-jquery-nonce", "security" );
	// Check user role
	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( esc_html__( 'You do not have permission to access this page.', 'shoprex-jquery' ) );
	}

	$field = explode( ",", $_POST['field'] );
	$value = explode( ",", $_POST['value'] );

	foreach ( $field as $key => $string ) {
		$result = shoprex_jquery_set_db_value( $string, $value[ $key ] );
		if ( $result == false ) {
			break;
		}
	}

	if ( $result ) {
		wp_send_json_success();
	} else {
		wp_send_json_error();
	}

}

add_action( 'wp_ajax_jquery_save_shoprex_settings', 'shoprex_jquery_save_settings' );

// Register Hook
function shoprex_jquery_activate_plugin() {
	register_uninstall_hook( __FILE__, 'shoprex_jquery_uninstall_plugin' );
}

register_activation_hook( __FILE__, 'shoprex_jquery_activate_plugin' );

// Uninstall Callback
function shoprex_jquery_uninstall_plugin() {
	// Include data file
	require( plugin_dir_path( __FILE__ ) . "shoprex-data.php" );

	foreach ( $shoprex_jquery_main_plugins as $array ) {
		shoprex_jquery_delete_db_entry( "shoprex_load_" . $array['display_name'] );
	}
	foreach ( $shoprex_jquery_plugins as $array ) {
		shoprex_jquery_delete_db_entry( "shoprex_load_" . $array['display_name'] );
	}
	foreach ( $shoprex_jquery_ui_plugins as $array ) {
		shoprex_jquery_delete_db_entry( "shoprex_load_" . $array['display_name'] );
	}
	foreach ( $shoprex_jquery_effect_plugins as $array ) {
		shoprex_jquery_delete_db_entry( "shoprex_load_" . $array['display_name'] );
	}
}


// Load Javascript and Styles in Frontend
function shoprex_jquery_enqueue_theme_script_and_styles() {

	$js_to_load = array();

	// Include data file
	require( plugin_dir_path( __FILE__ ) . "shoprex-data.php" );

	foreach ( $shoprex_jquery_main_plugins as $array ) {
		if ( ( $result = get_option( "shoprex_load_" . shoprex_jquery_format_db_keyname( $array['display_name'] ) ) ) != false ) {
			$js_to_load[] = $array['display_name'];
		}
	}
	foreach ( $shoprex_jquery_plugins as $array ) {
		if ( ( $result = get_option( "shoprex_load_" . shoprex_jquery_format_db_keyname( $array['display_name'] ) ) ) != false ) {
			$js_to_load[] = $array['display_name'];
		}
	}
	foreach ( $shoprex_jquery_ui_plugins as $array ) {
		if ( ( $result = get_option( "shoprex_load_" . shoprex_jquery_format_db_keyname( $array['display_name'] ) ) ) != false ) {
			$js_to_load[] = $array['display_name'];
		}
	}
	foreach ( $shoprex_jquery_effect_plugins as $array ) {
		if ( ( $result = get_option( "shoprex_load_" . shoprex_jquery_format_db_keyname( $array['display_name'] ) ) ) != false ) {
			$js_to_load[] = $array['display_name'];
		}
	}

	if ( ! empty( $js_to_load ) ) {
		wp_register_script( 'shoprex-jquery', plugin_dir_url( __FILE__ ) . 'js/shoprex-jquery-dummy.js', $js_to_load, false, true );
		wp_enqueue_script( 'shoprex-jquery' );
	}


}
add_action( 'wp_enqueue_scripts', 'shoprex_jquery_enqueue_theme_script_and_styles' );

